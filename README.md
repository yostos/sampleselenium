# Web画面のテスト

## スクリーンショットの取得

Seleniumを使用して循環しつつ各画面のスクリーンショットを取得するサンプルです。
現時点では３画面程度をカバーしています。

test.propertiesに有効なuseridとpasswordを設定してください。

実行は以下のコマンドです。Gradleを使用しているので依存ライブラリのダウンロード、コンパイル、
実行まで自動でやってくれます。実行できる環境の条件は以下です。

- Gradleの実行環境があること
- インターネットに接続できること（依存ライブラリのダウンロードを行うため）
- ChromeのWebドライバーは自身でインストールして億必要が有ります。インストールしたディレクトリを test.propertiesに反映してください。

お客様環境では、インターネット接続は難しいと思いますので、ライブラリを持ち込む必要が有ります。

実行方法

```sh
$ graddle run

> Task :run
Starting ChromeDriver 87.0.4280.20 (c99e81631faa0b2a448e658c0dbd8311fb04ddbd-refs/branch-heads/4280@{#355}) on port 22798
Only local connections are allowed.
Please see https://chromedriver.chromium.org/security-considerations for suggestions on keeping ChromeDriver safe.
ChromeDriver was started successfully.
12月 01, 2020 3:09:23 午後 org.openqa.selenium.remote.ProtocolHandshake createSession
情報: Detected dialect: W3C
Deleteing the image folder...
Test is starting....
Screenshots have been saved in "./images" folder.
Test has been successfully completed.
```


### Macの場合

- Chromedriverは 'brew install chromedriver'でOK。インストール先は '/usr/local/bin/chromedriver'。ただし、以下のようなセキュリティエラーが最初は出るので環境設定から実行を許可する。

![セキュリティエラー](docs/images/securityError.png)


## 画像の比較

上記の方法で取得した画面のスクリーンショットを、修正前と修正後で比較する必要があります。

ImageMagickを利用して簡単に比較することが可能です。

サンプル

```sh
$ ./compare.sh
```

上記のコマンドを実行すると、sample_images フォルダ内のa.png, b.png, c.pngを比較hし差分
イメージを出力してくれます。(a.pngとc.pngは同じ画像)
