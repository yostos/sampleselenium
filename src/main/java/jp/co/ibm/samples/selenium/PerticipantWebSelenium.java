package jp.co.ibm.samples.selenium;

import java.io.*;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.nio.file.FileAlreadyExistsException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.Dimension;
/**
 * PCWEBの各画面を巡回するクラス。
 * @author Yoshida
 */
public class PerticipantWebSelenium {

    private String image_dir = null;
    private WebDriver driver = null;
    private long bideTime = 0;
    private String userid = null;
    private String password = null;
    private Properties properties = null;
    private boolean swScreenshot = false;

    /**
     * Constructor
     * @param proertyFileName 使用するプロパティファイル名
     */
    public PerticipantWebSelenium( String propertyFileName ) {
        properties = new Properties();
        FileInputStream propfile = null;
        try {
            propfile = new FileInputStream(propertyFileName);
            properties.load(propfile);
        } catch(FileNotFoundException e) {
            System.out.println("Properties file was not found.");
        } catch(IOException e) {
            System.out.println("Failed to read the properties file.");
        }
        finally {
            if (propfile != null) {
                try {
                    propfile.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.setProperty("webdriver.chrome.driver"
                , properties.getProperty("path.webdriver"));
        String userAgent = properties.getProperty("chrome.options.useragent");
        log("User Agent = " + userAgent);
        if (Objects.nonNull(userAgent)) {
            //Useragentを指定
            ChromeOptions options = new ChromeOptions();
            options.addArguments(userAgent);
            driver = new ChromeDriver(options);
        } else {
            // No useragent is specified.
            driver = new ChromeDriver();
        }
        int dimensionX = Integer.parseInt(properties.getProperty("dimension.x"));
        int dimensionY = Integer.parseInt(properties.getProperty("dimension.y"));

        driver.manage().window().setSize(new Dimension(dimensionX,dimensionY));
        bideTime = Long.parseLong(properties.getProperty("bidetime"));
        userid = properties.getProperty("userid");
        password = properties.getProperty("password");
        image_dir = properties.getProperty("dir.images");
        log("Deleteing the image folder...");
        File imagedir = new File(image_dir);
        if (imagedir.exists()) {
            deleteDir(imagedir);
        }
        if (properties.getProperty("switch.screenshot").toUpperCase().equals("TRUE")) {
            swScreenshot = true;
            log("Screenshot mode : ON");
        } else {
            swScreenshot = false;
            log("Screenshot mode : OFF");
        }
        if (swScreenshot) {
            log("Creating the image folder...");
            imagedir.mkdir();
        }
    }

    /**
     * スクリーンショット出力用のディレクトリを削除
     * @param dir 削除対象ディレクトリ名
     */
    private void deleteDir(File dir) {
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            for(int i=0; i<files.length; i++) {
                deleteDir(files[i]);
            }
        }
        dir.delete();
    }

    /**
     * ログ出力
     * @param msg 出力するメッセージ内容
     */
    private void log(String msg) {
        // 現在日時情報で初期化されたインスタンスの取得
        LocalDateTime nowDateTime = LocalDateTime.now(); 
        DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        // 日時情報を指定フォーマットの文字列で取得
        String timeDisp = nowDateTime.format( timeFormat );
        System.out.println(timeDisp + " " + msg);
    }

    /**
     * ログイン画面に遷移する
     */
    public void login() {
        driver.get("https://www.nrkn.co.jp/rk/login.html");
        bide();
        screenshot("login");
        //ユーザーID入力
        driver.findElement(By.name("userId")).sendKeys(userid);
        //パスワード入力
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.id("btnLogin")).click();
        bide();
        screenshot("mainmenu");
    }

    /**
     * メールアドレス管理画面に遷移する
     */
    public void mailManagement() {
        driver.findElement(By.id("mainMenu10")).click();
        screenshot("mailMangement");
        driver.findElement(By.id("sNav01")).click();
    }

    /**
     * 電子帳票管理画面に遷移する
     */
    public void eForms(){
        driver.findElement(By.id("mainMenu11")).click();
        screenshot("eForms");
        driver.findElement(By.id("sNav01")).click();
    }

    /**
     * ログオフする
     */
    public void logoff() {
        driver.findElement(By.id("sNav03")).click();
        bide();
    }

    /**
     * Chrome Driverを終了させる。
     */
    public void quit() {
        driver.quit();
        log("Screenshots have been saved in \"" + image_dir
                + "\" folder.");
        log("Test has been completed.");
    }

    /**
     * プロパティに指定された時間分だけ処理を停止する。
     */
    private void bide(){
        try {
            Thread.sleep(bideTime);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * スクリーンショットを取得する。
     * 実際にはスクリーンショット用出力先ディレクトリ配下に指定されたファイル名で
     * サブフォルダを作成し出力する。
     * @param dir 書き出し先のファイル名。
     */
    private void screenshot(String subdir) {
        if (!swScreenshot) {
            return;
        }
        String saveDir = image_dir + File.separator + subdir;

        File dir = new File(saveDir);
        dir.mkdir();

        BigDecimal scrollYAfter = BigDecimal.valueOf(-1);

        int count = 1;
        // スクロールできなくなる(スクリーン位置が同じになる)まで繰り返す
        for (BigDecimal scrollYBefore = this.getScrollY(driver);
                (scrollYBefore != null && !scrollYBefore.equals(scrollYAfter));
                scrollYBefore = this.moveScrollY(driver)) {
            File screenFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                this.copy(screenFile, new File(saveDir, "screenshot_" + count + ".png"), true);
                count++;
                scrollYAfter = scrollYBefore;
            } catch(IOException e) {
                e.printStackTrace();
            }

                }
    }
    /**
     * 現在の縦スクロール位置を取得する。
     * @param driver WebDriver
     * @return スクロール位置
     */
    private BigDecimal getScrollY(WebDriver driver) {
        String scrollY = (String) ((JavascriptExecutor) driver).executeScript(
                "return String(window.pageYOffset);");
        BigDecimal value = null;
        if (scrollY != null && scrollY.matches("^[0-9]+$")) {
            value = new BigDecimal(scrollY);
        }
        return value;
    }

    /**
     * 画面サイズに合わせてスクロールを移動する
     * @param driver WebDriver
     * @return 移動後のスクロール位置
     */
    private BigDecimal moveScrollY(WebDriver driver) {

        // 画面サイズを取得
        int height = driver.manage().window().getSize().height;

        // 見切れて画面確認ができない箇所ができないようにするため、
        // 画面サイズより少し小さい値でスクロールさせる
        String scrollY = (String) ((JavascriptExecutor) driver).executeScript(
                "scrollBy( 0, " + 
                (height - 45) + " ); return String(window.pageYOffset);");
        BigDecimal value = null;
        if (scrollY != null && scrollY.matches("^[0-9]+$")) {
            value = new BigDecimal(scrollY);
        }

        return value;
    }

    /**
     * ファイルをコピーする
     * @param fromFile コピー元ファイル
     * @param toFile コピー先ファイル
     * @param overwrite 上書きフラグ(true:上書きする, false:既に存在した場合、IOExceptionが発生)
     * @throws IOException ファイルコピーに失敗した場合に発生
     */
    private void copy(File fromFile, File toFile, boolean overwrite) throws IOException {
        if (fromFile == null) {
            throw new IllegalArgumentException("param[fromFile] is null");
        }
        if (toFile == null) {
            throw new IllegalArgumentException("param[fromFile] is null");
        }
        if (!fromFile.exists()) {
            throw new FileNotFoundException("There is not fromFile[" +
                    fromFile.getAbsolutePath() + "].");
        }
        if (toFile.exists() && !overwrite) {
            throw new FileAlreadyExistsException(toFile.getAbsolutePath());
        }

        try (BufferedInputStream in =
                new BufferedInputStream(new FileInputStream(fromFile))) {
            try (BufferedOutputStream out = new BufferedOutputStream(
                        new FileOutputStream(toFile))) {
                byte[] buff = new byte[1024];
                for (int size = in.read(buff); size >= 0; size = in.read(buff)) {
                    out.write(buff, 0, size);
                }
                        }
                }
    }
}
