package jp.co.ibm.samples.selenium;

public class TestCase1 {
    public static void main(String[] args) {
        String propertyFile = "testcase1.properties";
        PerticipantWebSelenium selenium = new PerticipantWebSelenium(propertyFile);
        selenium.login();
        selenium.eForms();
        selenium.mailManagement();
        selenium.logoff();
        selenium.quit();
    }
}
